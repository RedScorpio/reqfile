'use strict';

var Fs = require('fs'),
  Path = require('path');
var root = Path.resolve('./');

var requirable = [];

var ignored = [
  '/.git', '/node_modules'
];
var acceptedEndings = [
  '.js', '.json'
];

function exists(p) {
  var stat;
  try {
    stat = Fs.lstatSync(p);
  } catch (err) {
    return false;
  }
  return stat.isDirectory() || stat.isFile();
}

function hasAcceptedEnding(file) {
  for (var i = 0; i < acceptedEndings.length; i++) {
    if (file.endsWith(acceptedEndings[i])) {
      return true;
    }
  }
  return false;
}

function scanFiles(findPath) {
  var configPath = Path.join(root, '.reqfileconfig');
  if (exists(configPath)) {
    var config = require(configPath);
    if (Array.isArray(config.exclude)) {
      ignored.push.apply(ignored, config.exclude);
    }
    if (Array.isArray(config.include)) {
      acceptedEndings.push.apply(acceptedEndings, config.include);
    }
  }

  for (var i = 0; i < ignored.length; i++) {
    ignored[i] = Path.normalize(ignored[i]);
  }

  Fs.readdirSync(findPath).forEach(function (file) {
    var path = Path.join(findPath, file);
    for (var i = 0; i < ignored.length; i++) {
      if (path.indexOf(ignored[i]) >= 0) {
        return;
      }
    }
    var stat = Fs.lstatSync(path);
    if (stat.isFile() && hasAcceptedEnding(file)) {
      requirable.push(path.substring(0, path.length - 3));
    } else if (stat.isDirectory()) {
      scanFiles(path);
    }
  });
}

scanFiles(root);

module.exports = function (name) {
  var paths = requirable.filter(function (filePath) {
    return filePath.endsWith(Path.normalize('/' + name));
  });
  if (paths.length === 1) {
    return paths[0];
  } else if (paths.length === 0) {
    throw new Error('reqfile: ' + name + '\n\tRequired file not found.')
  } else {
    throw new Error('reqfile: ' + name + '\n\tThere is more than one file with that name in your project:\n\t\t' + paths.join('\n\t\t'));
  }
};
