# reqfile

Simple module that allows to require files by name.

To require file by name or it's part, only one file with such name is allowed to exist in project.

## Usage

By default only `.js` and `.json` files are allowed to be included and `node_modules` and `.git` folders are excluded.
	
	var reqfile = require('reqfile'),
	  userController = require(reqfile('user.server.controller'));

To customize inclusion/exclusion list, include `.reqfileconfig` file in project's root.

	module.exports = {
	  include: ['.html'],
	  exclude: ['/public', '/typings']
	};
